DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `precio` float DEFAULT '0',
  `fotografia` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),