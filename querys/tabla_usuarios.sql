CREATE TABLE IF NOT EXISTS `users`(
    `id` INT(8) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(30) NOT NULL,
    `email` VARCHAR(60) NOT NULL,
    `password` VARCHAR(40) NOT NULL,
    `estado` INT(8),
    PRIMARY KEY(`id`),
    UNIQUE KEY `email`(`email`)
)